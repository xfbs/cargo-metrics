# Cargo Metrics

This is a little tool that extracts metrics from your cargo project and outputs
them in a format that GitLab can understand.

Why do you need this? It means that you can immediately see if an important
metric has changed in merge requests.


## Setup

You can install and run it locally using cargo.

    cargo install cargo-metrics
    cargo metrics

To integrate it into your GitLab CI, add a job to your `.gitlab-ci.yml` that
looks like this:

```yml
metrics:
  image: rust
  stage: publish
  script:
    - cargo install cargo-metrics
    - cargo metrics > metrics.txt
  artifacts:
    reports:
      metrics: metrics.txt
```

What this will do is it will generate these metrics on every commit, and pass
them onto GitLab. When merge requests are created, it is also run, and GitLab
will show you which metrics have changed between the `master` branch and the
merge request.

## License

MIT.
